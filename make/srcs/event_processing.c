/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_processing.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/08 10:57:15 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/25 15:34:38 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

int		destroy_win(t_vars *vars)
{
	mlx_destroy_window(vars->mlx, vars->mlx_win);
	if (vars->map)
		free(vars->map);
	if (vars->head)
		free_tmp(&(vars->head));
	if (vars->lst)
		free_sprite(&(vars->lst));
	if (vars->img_s.img_path != NULL && vars->flag_s == 1)
		free(vars->img_s.img_path);
	if (vars->img_n.img_path != NULL && vars->flag_n == 1)
		free(vars->img_n.img_path);
	if (vars->img_w.img_path != NULL && vars->flag_w == 1)
		free(vars->img_w.img_path);
	if (vars->img_e.img_path != NULL && vars->flag_e == 1)
		free(vars->img_e.img_path);
	if (vars->img_sprite.img_path != NULL && vars->flag_p == 1)
		free(vars->img_sprite.img_path);
	exit(0);
	return (0);
}

void	render_next_frame(t_vars *vars)
{
	mlx_destroy_image(vars->mlx, vars->img_win.img);
	if (!(vars->img_win.img = mlx_new_image(vars->mlx,
	vars->win_w, vars->win_h)))
		check_error(vars, 3, MLX_ERROR);
	vars->img_win.addrs = mlx_get_data_addr(vars->img_win.img,
	&vars->img_win.bits_per_pixel,
	&vars->img_win.line_length, &vars->img_win.endian);
	draw_floor(vars);
	draw_map(vars);
	mlx_put_image_to_window(vars->mlx, vars->mlx_win, vars->img_win.img, 0, 0);
}

int		event_proc(int keycode, t_vars *vars)
{
	if (keycode == 65307)
	{
		destroy_win(vars);
	}
	if (keycode == 65361)
		check_event_third(vars, 'l');
	else if (keycode == 65363)
		check_event_third(vars, 'r');
	if (keycode == 119)
		check_event(vars, 'w');
	else if (keycode == 97)
		check_event(vars, 'a');
	else if (keycode == 115)
		check_event_sec(vars, 's');
	else if (keycode == 100)
		check_event_sec(vars, 'd');
	render_next_frame(vars);
	return (1);
}
