/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_processing.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/08 10:31:26 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/24 11:55:52 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

void			draw_floor(t_vars *vars)
{
	int i;
	int j;

	i = 0;
	while (i++ < vars->win_h / 2)
	{
		j = 0;
		while (j < vars->win_w)
		{
			my_mlx_pxl_put(vars, j, i, create_trgb(1, vars->img_floor_ceill.r,
			vars->img_floor_ceill.g, vars->img_floor_ceill.b));
			j++;
		}
	}
	while (i++ < vars->win_h)
	{
		j = 0;
		while (j < vars->win_w)
		{
			my_mlx_pxl_put(vars, j, i, create_trgb(1, vars->img_floor_ceill.r_c,
			vars->img_floor_ceill.g_c, vars->img_floor_ceill.b_c));
			j++;
		}
	}
}

static void		check_else_str(t_vars *vars, int i, int j)
{
	while (vars->map[i] != NULL)
	{
		j = 0;
		while (vars->map[i][j] != '\0')
		{
			if (vars->map[i][j] == ' ' || vars->map[i][j] == '\n'
			|| vars->map[i][j] == '\t' || vars->map[i][j] == '\0')
				j++;
			else
				check_error(vars, 3, MLX_ERROR);
		}
		i++;
	}
}

static void		check_map_spaces(t_vars *vars)
{
	int flag;
	int i;
	int j;

	flag = 0;
	i = 0;
	j = 0;
	while (vars->map[i] != NULL)
	{
		j = 0;
		while (vars->map[i][j] != '\0')
			j++;
		if (j == 0 || vars->map[i][j - 1] == '\t')
		{
			flag = 1;
			break ;
		}
		i++;
	}
	if (flag == 1)
		check_else_str(vars, i, j);
}

static void		map_processing(t_vars *vars, int i, int j)
{
	if (i < 0 || j < 0 || i >= vars->check_params
	|| (size_t)(j) >= ft_strlen(vars->map[i]) || vars->map[i][j] == ' ')
		check_error(vars, 3, MAP_ERROR);
	if (vars->map[i][j] == '.' || vars->map[i][j] == '1'
	|| vars->map[i][j] == 's')
		return ;
	if (vars->map[i][j] == '0')
		vars->map[i][j] = '.';
	if (vars->map[i][j] == '2')
	{
		vars->map[i][j] = 's';
		vars->sprites++;
		spriteadd_back(&(vars->lst), spritenew(vars, j + .5, i + .5));
	}
	map_processing(vars, i + 1, j);
	map_processing(vars, i - 1, j);
	map_processing(vars, i, j + 1);
	map_processing(vars, i, j - 1);
}

void			check_map_all(t_vars *vars)
{
	int i;
	int j;

	i = vars->player_y;
	j = vars->player_x;
	map_processing(vars, i, j);
	check_map_spaces(vars);
}
