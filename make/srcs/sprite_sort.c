/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite_sort.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/18 14:42:33 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/24 12:00:58 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

t_sprite		*spritenew(t_vars *vars, float x, float y)
{
	t_sprite	*new;

	if (!(new = (t_sprite *)malloc(sizeof(t_sprite))))
		check_error(vars, 3, MEMORY_ERROR);
	new->x = x;
	new->y = y;
	new->next = NULL;
	return (new);
}

void			spriteadd_back(t_sprite **head, t_sprite *new)
{
	t_sprite	*tmp;

	if (*head)
	{
		tmp = *head;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
	else
		*head = new;
}

static void		m_swap(float *a, float *b)
{
	float	tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

static t_sprite	*farest(t_sprite *head, t_vars *vars)
{
	t_sprite	*farest;
	float		max;
	float		tmp;

	farest = head;
	max = (head->x - vars->player_x) * (head->x - vars->player_x) +
	(head->y - vars->player_y) * (head->y - vars->player_y);
	while (head)
	{
		tmp = (head->x - vars->player_x) * (head->x - vars->player_x) +
		(head->y - vars->player_y) * (head->y - vars->player_y);
		if (tmp > max)
		{
			max = tmp;
			farest = head;
		}
		head = head->next;
	}
	return (farest);
}

void			sprite_choice_sort(t_sprite *head, t_vars *vars)
{
	t_sprite *tmp;

	while (head)
	{
		tmp = farest(head, vars);
		m_swap(&(head->x), &(tmp->x));
		m_swap(&(head->y), &(tmp->y));
		head = head->next;
	}
}
