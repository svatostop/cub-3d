/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player_processing.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/02 11:08:28 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/30 11:51:23 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

static int		check_w_s(char *str, int i, t_vars *vars)
{
	if (str[i] == 'S')
	{
		vars->player_x = i + 0.5;
		vars->dir_y = 1;
		vars->dir_x = 0;
		vars->a_y = 0;
		vars->a_x = 1;
		return (1);
	}
	if (str[i] == 'W')
	{
		vars->player_x = i + 0.5;
		vars->dir_y = 0;
		vars->dir_x = -1;
		vars->a_y = 1;
		vars->a_x = 0;
		return (1);
	}
	return (0);
}

static int		check_e_n(char *str, int i, t_vars *vars)
{
	if (str[i] == 'N')
	{
		vars->player_x = i + 0.5;
		vars->dir_y = -1;
		vars->dir_x = 0;
		vars->a_y = 0;
		vars->a_x = -1;
		return (1);
	}
	if (str[i] == 'E')
	{
		vars->player_x = i + 0.5;
		vars->dir_y = 0;
		vars->dir_x = 1;
		vars->a_y = -1;
		vars->a_x = 0;
		return (1);
	}
	return (0);
}

static void		check_player_xy_sec(char *str, t_vars *vars, int j, int *check)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (check_w_s(str, i, vars) == 1)
		{
			vars->player_y = j + 0.5;
			str[i] = '0';
			(*check)++;
		}
		if (str[i] == ' ' || str[i] == '\t' || str[i] == '1'
		|| str[i] == '0' || str[i] == '2' || str[i] == 'E' || str[i] == 'N')
			i++;
		else
			check_error(vars, 3, MAP_ERROR);
	}
}

static void		check_player_xy(char *str, t_vars *vars, int j, int *check)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (check_e_n(str, i, vars) == 1)
		{
			vars->player_y = j + 0.5;
			str[i] = '0';
			(*check)++;
		}
		if (str[i] == ' ' || str[i] == '\t' || str[i] == '1'
		|| str[i] == '0' || str[i] == '2' || str[i] == 'S' || str[i] == 'W')
			i++;
		else
			check_error(vars, 3, MAP_ERROR);
	}
}

void			get_player_xy(t_vars *vars)
{
	int i;
	int y;
	int check;

	i = 0;
	y = 0;
	check = 0;
	while (vars->map[i] != NULL)
	{
		y = i;
		check_player_xy(vars->map[i], vars, y, &check);
		check_player_xy_sec(vars->map[i], vars, y, &check);
		i++;
	}
	vars->check_params = i;
	if (check > 1 || check == 0)
		check_error(vars, 3, MAP_ERROR);
}
