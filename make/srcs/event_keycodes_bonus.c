/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_keycodes_bonus.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 16:09:21 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/24 19:27:18 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

void		check_event(t_vars *vars, char mode)
{
	if (mode == 'w')
	{
		if (vars->map[(int)vars->player_y]
			[(int)(vars->player_x + (3 * .2 * vars->dir_x))] == '.')
			vars->player_x += .2 * vars->dir_x;
		if (vars->map[(int)(vars->player_y + (3 * .2 * vars->dir_y))]
			[(int)vars->player_x] == '.')
			vars->player_y += .2 * vars->dir_y;
	}
	if (mode == 'a')
	{
		if (vars->map[(int)vars->player_y]
			[(int)(vars->player_x + (3 * .2 * vars->dir_y))] == '.')
			vars->player_x += .2 * vars->dir_y;
		if (vars->map[(int)(vars->player_y - (3 * .2 * vars->dir_x))]
			[(int)vars->player_x] == '.')
			vars->player_y -= .2 * vars->dir_x;
	}
}

void		check_event_sec(t_vars *vars, char mode)
{
	if (mode == 's')
	{
		if (vars->map[(int)vars->player_y]
			[(int)(vars->player_x - (3 * .2 * vars->dir_x))] == '.')
			vars->player_x -= .2 * vars->dir_x;
		if (vars->map[(int)(vars->player_y - (3 * .2 * vars->dir_y))]
			[(int)vars->player_x] == '.')
			vars->player_y -= .2 * vars->dir_y;
	}
	if (mode == 'd')
	{
		if (vars->map[(int)vars->player_y]
			[(int)(vars->player_x - (3 * .2 * vars->dir_y))] == '.')
			vars->player_x -= .2 * vars->dir_y;
		if (vars->map[(int)(vars->player_y + (3 * .2 * vars->dir_x))]
			[(int)vars->player_x] == '.')
			vars->player_y += .2 * vars->dir_x;
	}
}

void		check_event_third(t_vars *vars, char mode)
{
	float dir_x;
	float a_x;
	float alpha;

	alpha = .1;
	dir_x = vars->dir_x;
	a_x = vars->a_x;
	if (mode == 'r')
	{
		vars->dir_x = vars->dir_x * cos(alpha) - vars->dir_y * sin(alpha);
		vars->dir_y = dir_x * sin(alpha) + vars->dir_y * cos(alpha);
		vars->a_x = vars->a_x * cos(alpha) - vars->a_y * sin(alpha);
		vars->a_y = a_x * sin(alpha) + vars->a_y * cos(alpha);
	}
	if (mode == 'l')
	{
		vars->dir_x = vars->dir_x * cos(-alpha) - vars->dir_y * sin(-alpha);
		vars->dir_y = dir_x * sin(-alpha) + vars->dir_y * cos(-alpha);
		vars->a_x = vars->a_x * cos(-alpha) - vars->a_y * sin(-alpha);
		vars->a_y = a_x * sin(-alpha) + vars->a_y * cos(-alpha);
	}
}
