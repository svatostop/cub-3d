/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_lines_for_walls.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svatostop <svatostop@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/08 11:28:22 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/20 16:51:24 by svatostop        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

static void		check_texture_for_walls(t_vars *vars, float *k)
{
	if (vars->mode == 'w')
	{
		*k = vars->img_w.img_h / (float)vars->view_h;
		vars->img_x = vars->img_x * vars->img_w.img_w;
	}
	if (vars->mode == 'e')
	{
		*k = vars->img_e.img_h / (float)vars->view_h;
		vars->img_x = vars->img_x * vars->img_e.img_w;
	}
	if (vars->mode == 'n')
	{
		*k = vars->img_n.img_h / (float)vars->view_h;
		vars->img_x = vars->img_x * vars->img_n.img_w;
	}
	if (vars->mode == 's')
	{
		*k = vars->img_s.img_h / (float)vars->view_h;
		vars->img_x = vars->img_x * vars->img_s.img_w;
	}
}

void			draw_line(t_vars *vars, int x)
{
	int		start;
	int		img_start;
	int		end;
	int		begin;
	float	k;

	start = ((vars->win_h / 2) - (vars->view_h / 2));
	img_start = 0;
	k = 0;
	if (start < 0)
	{
		img_start = -start;
		start = 0;
	}
	end = start + vars->view_h + img_start;
	if (end >= vars->win_h)
		end = vars->win_h - 1;
	begin = start;
	check_texture_for_walls(vars, &k);
	while (start < end)
	{
		my_mlx_pxl_put(vars, x, start, get_pixel_clr(vars,
			(int)(vars->img_x), (int)((start - begin + img_start) * k)));
		start++;
	}
}
