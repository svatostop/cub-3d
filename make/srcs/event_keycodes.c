/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_keycodes.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 16:09:21 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/30 11:50:53 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

static int	check_char(char c)
{
	char	*str;
	int		i;

	str = "1.s";
	i = 0;
	while (str[i])
	{
		if (c == str[i])
			return (1);
		i++;
	}
	return (0);
}

void		check_event(t_vars *vars, char mode)
{
	if (mode == 'w')
	{
		if (check_char(vars->map[(int)vars->player_y]
			[(int)(vars->player_x + (vars->dir_x))]) == 1)
			vars->player_x += .2 * vars->dir_x;
		if (check_char(vars->map[(int)(vars->player_y + (vars->dir_y))]
			[(int)vars->player_x]) == 1)
			vars->player_y += .2 * vars->dir_y;
	}
	if (mode == 'a')
	{
		if (check_char(vars->map[(int)vars->player_y]
			[(int)(vars->player_x + (vars->dir_y))]) == 1)
			vars->player_x += .2 * vars->dir_y;
		if (check_char(vars->map[(int)(vars->player_y - (vars->dir_x))]
			[(int)vars->player_x]) == 1)
			vars->player_y -= .2 * vars->dir_x;
	}
}

void		check_event_sec(t_vars *vars, char mode)
{
	if (mode == 's')
	{
		if (check_char(vars->map[(int)vars->player_y]
			[(int)(vars->player_x - (vars->dir_x))]) == 1)
			vars->player_x -= .2 * vars->dir_x;
		if (check_char(vars->map[(int)(vars->player_y - (vars->dir_y))]
			[(int)vars->player_x]) == 1)
			vars->player_y -= .2 * vars->dir_y;
	}
	if (mode == 'd')
	{
		if (check_char(vars->map[(int)vars->player_y]
			[(int)(vars->player_x - (vars->dir_y))]) == 1)
			vars->player_x -= .2 * vars->dir_y;
		if (check_char(vars->map[(int)(vars->player_y + (vars->dir_x))]
			[(int)vars->player_x]) == 1)
			vars->player_y += .2 * vars->dir_x;
	}
}

void		check_event_third(t_vars *vars, char mode)
{
	float dir_x;
	float a_x;
	float alpha;

	alpha = .1;
	dir_x = vars->dir_x;
	a_x = vars->a_x;
	if (mode == 'r')
	{
		vars->dir_x = vars->dir_x * cos(alpha) - vars->dir_y * sin(alpha);
		vars->dir_y = dir_x * sin(alpha) + vars->dir_y * cos(alpha);
		vars->a_x = vars->a_x * cos(alpha) - vars->a_y * sin(alpha);
		vars->a_y = a_x * sin(alpha) + vars->a_y * cos(alpha);
	}
	if (mode == 'l')
	{
		vars->dir_x = vars->dir_x * cos(-alpha) - vars->dir_y * sin(-alpha);
		vars->dir_y = dir_x * sin(-alpha) + vars->dir_y * cos(-alpha);
		vars->a_x = vars->a_x * cos(-alpha) - vars->a_y * sin(-alpha);
		vars->a_y = a_x * sin(-alpha) + vars->a_y * cos(-alpha);
	}
}
