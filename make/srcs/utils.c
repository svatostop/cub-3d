/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/31 12:26:50 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/24 14:35:19 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

int				my_atoi(const char *str, int *i, t_vars *vars)
{
	int num;

	num = 0;
	if (str[(*i)] == 'R' || str[(*i)] == 'F' || str[(*i)] == 'C')
		(*i)++;
	while (str[(*i)] != '\0' && (str[(*i)] == ' '
	|| str[(*i)] == '\n' || str[(*i)] == '\t'
	|| str[(*i)] == '\f' || str[(*i)] == '\v' || str[(*i)] == '\r'))
		(*i)++;
	if (str[(*i)] == '-' || str[(*i)] == '+')
		check_error(vars, 3, MAP_ARGS_ERROR);
	if (ft_isdigit(str[(*i)]) == 0)
		check_error(vars, 3, MAP_ARGS_ERROR);
	while (str[(*i)] >= '0' && str[(*i)] <= '9')
	{
		num = num * 10 + (str[(*i)] - '0');
		(*i)++;
	}
	return (num);
}

void			my_mlx_pxl_put(t_vars *vars, int x, int y, int color)
{
	char *ptr;

	ptr = vars->img_win.addrs +
				(y * vars->img_win.line_length + x *
				(vars->img_win.bits_per_pixel / 8));
	*(unsigned int*)ptr = color;
}

int				get_pixel_clr(t_vars *vars, int x, int y)
{
	int		color;
	char	*ptr;

	if (vars->mode == 'w')
		ptr = vars->img_w.addrs + (y * vars->img_w.line_length +
		x * (vars->img_w.bits_per_pixel / 8));
	if (vars->mode == 'n')
		ptr = vars->img_n.addrs + (y * vars->img_n.line_length +
		x * (vars->img_n.bits_per_pixel / 8));
	if (vars->mode == 'e')
		ptr = vars->img_e.addrs + (y * vars->img_e.line_length +
		x * (vars->img_e.bits_per_pixel / 8));
	if (vars->mode == 's')
		ptr = vars->img_s.addrs + (y * vars->img_s.line_length +
		x * (vars->img_s.bits_per_pixel / 8));
	if (vars->mode == 'p')
		ptr = vars->img_sprite.addrs + (y * vars->img_sprite.line_length +
		x * (vars->img_sprite.bits_per_pixel / 8));
	color = *(unsigned int*)ptr;
	return (color);
}

int				create_trgb(int t, int r, int g, int b)
{
	return (t << 24 | r << 16 | g << 8 | b);
}

void			check_fd_args(int *fd, char **argv, int argc, t_vars *vars)
{
	if (argc == 2 || argc == 3)
	{
		*fd = open(argv[1], O_RDONLY);
		if (*fd == -1)
			check_error(vars, 1, FD_ERROR);
		if (!(vars->check_argv = ft_strdup(argv[1])))
			check_error(vars, 1, MEMORY_ERROR);
		if (cmp_str_argv(vars->check_argv, ".cub") != 0)
		{
			free(vars->check_argv);
			vars->check_argv = NULL;
			check_error(vars, 1, MAP_ARGS_ERROR);
		}
		free(vars->check_argv);
		vars->check_argv = NULL;
	}
	else
		check_error(vars, 1, ARGS_ERROR);
	if (argc == 3)
	{
		if (!(vars->check_argv = ft_strdup(argv[2])))
			check_error(vars, 1, MEMORY_ERROR);
		if (cmp_str(vars, "--save") == 1)
			vars->argv_flag = 1;
	}
}
