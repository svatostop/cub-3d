/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/22 18:03:09 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/25 15:41:32 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

static void		vars_init_sec(t_vars *vars)
{
	vars->flag_res = 0;
	vars->flag_w = 0;
	vars->flag_n = 0;
	vars->flag_s = 0;
	vars->flag_e = 0;
	vars->flag_p = 0;
	vars->flag_floor = 0;
	vars->flag_ceill = 0;
	vars->check_params = 0;
	vars->move_height = 1.5;
	vars->move_width = 1.5;
	vars->move = 120;
	vars->head = NULL;
	vars->tmp = NULL;
	vars->img_x = 0;
	vars->img_s.img_path = NULL;
	vars->img_n.img_path = NULL;
	vars->img_w.img_path = NULL;
	vars->img_e.img_path = NULL;
	vars->img_sprite.img_path = NULL;
}

static void		vars_init(t_vars *vars)
{
	vars->win_w = 500;
	vars->win_h = 900;
	vars->map = NULL;
	vars->lst = NULL;
	vars->sprites = 0;
	vars->player_x = 15;
	vars->player_y = 1;
	vars->dir_x = 0;
	vars->dir_y = -1;
	vars->a_x = -1;
	vars->a_y = 0;
	vars->view_h = 0;
	vars->flag = 0;
	vars->img_x = 0;
	vars->perp_dist = 0;
	vars->img_floor_ceill.r = 0;
	vars->img_floor_ceill.g = 0;
	vars->img_floor_ceill.b = 0;
	vars->argv_flag = 0;
	vars_init_sec(vars);
}

static void		init_vars_texture(t_vars *vars)
{
	vars->img_w.img = mlx_xpm_file_to_image(vars->mlx,
	vars->img_w.img_path, &vars->img_w.img_w, &vars->img_w.img_h);
	vars->img_n.img = mlx_xpm_file_to_image(vars->mlx,
	vars->img_n.img_path, &vars->img_n.img_w, &vars->img_n.img_h);
	vars->img_e.img = mlx_xpm_file_to_image(vars->mlx,
	vars->img_e.img_path, &vars->img_e.img_w, &vars->img_e.img_h);
	vars->img_s.img = mlx_xpm_file_to_image(vars->mlx,
	vars->img_s.img_path, &vars->img_s.img_w, &vars->img_s.img_h);
	vars->img_sprite.img = mlx_xpm_file_to_image(vars->mlx,
	vars->img_sprite.img_path,
	&vars->img_sprite.img_w, &vars->img_sprite.img_h);
	if (!vars->img_w.img || !vars->img_n.img || !vars->img_e.img
	|| !vars->img_s.img || !vars->img_sprite.img)
		check_error(vars, 3, MAP_ARGS_ERROR);
	vars->img_w.addrs = mlx_get_data_addr(vars->img_w.img,
	&vars->img_w.bits_per_pixel, &vars->img_w.line_length, &vars->img_w.endian);
	vars->img_n.addrs = mlx_get_data_addr(vars->img_n.img,
	&vars->img_n.bits_per_pixel, &vars->img_n.line_length, &vars->img_n.endian);
	vars->img_e.addrs = mlx_get_data_addr(vars->img_e.img,
	&vars->img_e.bits_per_pixel, &vars->img_e.line_length, &vars->img_e.endian);
	vars->img_s.addrs = mlx_get_data_addr(vars->img_s.img,
	&vars->img_s.bits_per_pixel, &vars->img_s.line_length, &vars->img_s.endian);
	vars->img_sprite.addrs = mlx_get_data_addr(vars->img_sprite.img,
	&vars->img_sprite.bits_per_pixel,
	&vars->img_sprite.line_length, &vars->img_sprite.endian);
}

int				main(int argc, char **argv)
{
	t_vars	vars;
	int		fd;

	vars_init(&vars);
	check_fd_args(&fd, argv, argc, &vars);
	parse_map(&vars, fd);
	close(fd);
	vars.mlx = mlx_init();
	init_vars_texture(&vars);
	if (!(vars.img_win.img = mlx_new_image(vars.mlx, vars.win_w, vars.win_h)))
		check_error(&vars, 3, MLX_ERROR);
	vars.img_win.addrs = mlx_get_data_addr(vars.img_win.img,
	&vars.img_win.bits_per_pixel, &vars.img_win.line_length,
	&vars.img_win.endian);
	draw_floor(&vars);
	draw_map(&vars);
	if (vars.argv_flag == 1)
		make_screenshot(&vars);
	if (!(vars.mlx_win = mlx_new_window(vars.mlx, vars.win_w, vars.win_h, "g")))
		check_error(&vars, 3, MLX_ERROR);
	mlx_put_image_to_window(vars.mlx, vars.mlx_win, vars.img_win.img, 0, 0);
	mlx_hook(vars.mlx_win, 2, 1L << 0, event_proc, &vars);
	mlx_hook(vars.mlx_win, 17, 1L << 17, destroy_win, &vars);
	mlx_loop(vars.mlx);
	return (0);
}
