/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/31 12:28:52 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/20 15:05:17 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

static void		params_for_dirs(t_vars *vars, float *ray_y, float *ray_x, int x)
{
	vars->map_x = (int)vars->player_x;
	vars->map_y = (int)vars->player_y;
	*ray_x = vars->dir_x + vars->a_x * (1 - 2 * x / (float)vars->win_w);
	*ray_y = vars->dir_y + vars->a_y * (1 - 2 * x / (float)vars->win_w);
	vars->dist_x = sqrt(1 + ((*ray_y * *ray_y) / (*ray_x * *ray_x)));
	vars->dist_y = sqrt(1 + ((*ray_x * *ray_x) / (*ray_y * *ray_y)));
	if (*ray_x > 0)
		vars->step_x = (vars->map_x + 1 - vars->player_x) * vars->dist_x;
	else
		vars->step_x = (vars->player_x - vars->map_x) * vars->dist_x;
	if (*ray_y < 0)
		vars->step_y = (vars->player_y - vars->map_y) * vars->dist_y;
	else
		vars->step_y = (vars->map_y + 1 - vars->player_y) * vars->dist_y;
}

static int		check_steps(t_vars *vars, float ray_y, float ray_x, int side)
{
	if (vars->step_x < vars->step_y)
	{
		if (ray_x > 0)
			vars->map_x++;
		else
			vars->map_x--;
		vars->step_x += vars->dist_x;
		side = 0;
	}
	else
	{
		if (ray_y > 0)
			vars->map_y++;
		else
			vars->map_y--;
		vars->step_y += vars->dist_y;
		side = 1;
	}
	return (side);
}

static void		check_first_side(t_vars *vars, float ray_y, float ray_x)
{
	float hit_xy;
	float unkw_side;

	vars->step_y -= vars->dist_y;
	unkw_side = ray_x * (vars->step_y / sqrt(ray_x * ray_x + ray_y * ray_y));
	hit_xy = vars->player_x + unkw_side;
	if (ray_y > 0)
	{
		vars->mode = 's';
		vars->img_x = 1 - (hit_xy - (int)hit_xy);
		vars->perp_dist = (vars->map_y - vars->player_y) / ray_y;
	}
	else
	{
		vars->mode = 'n';
		vars->img_x = hit_xy - (int)hit_xy;
		vars->perp_dist = (vars->map_y + 1 - vars->player_y) / ray_y;
	}
}

static void		check_sec_side(t_vars *vars, float ray_y, float ray_x)
{
	float hit_xy;
	float unkw_side;

	vars->step_x -= vars->dist_x;
	unkw_side = ray_y * (vars->step_x / sqrt(ray_x * ray_x + ray_y * ray_y));
	hit_xy = vars->player_y + unkw_side;
	if (ray_x > 0)
	{
		vars->mode = 'e';
		vars->img_x = hit_xy - (int)hit_xy;
		vars->perp_dist = (vars->map_x - vars->player_x) / ray_x;
	}
	else
	{
		vars->mode = 'w';
		vars->img_x = 1 - (hit_xy - (int)hit_xy);
		vars->perp_dist = (vars->map_x + 1 - vars->player_x) / ray_x;
	}
}

void			draw_map(t_vars *vars)
{
	int		side;
	float	ray_x;
	float	ray_y;
	int		x;
	float	zbuff[vars->win_w];

	side = 0;
	ray_x = 0;
	ray_y = 0;
	x = 0;
	while (x < vars->win_w)
	{
		params_for_dirs(vars, &ray_y, &ray_x, x);
		while (vars->map[vars->map_y][vars->map_x] != '1')
			side = check_steps(vars, ray_y, ray_x, side);
		if (side == 1)
			check_first_side(vars, ray_y, ray_x);
		else
			check_sec_side(vars, ray_y, ray_x);
		vars->view_h = (int)(vars->win_h / vars->perp_dist);
		draw_line(vars, x);
		zbuff[x] = vars->perp_dist;
		x++;
	}
	draw_sprite(vars, zbuff);
}
