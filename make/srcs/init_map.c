/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/31 11:32:20 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/30 11:58:05 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

static void		resolution_check(t_vars *vars, char *str, int *i)
{
	int		j;
	char	*tm;

	j = *i;
	if (str[(*i)] == 'S' && str[(*i) + 1] == ' ')
	{
		j += 2;
		tm = vars->img_sprite.img_path;
		vars->img_sprite.img_path = ft_strtrim(str + j, "\t ");
		free(tm);
		vars->flag_p = 1;
		vars->check_params++;
	}
	if (str[(*i)] == 'R')
	{
		vars->win_w = my_atoi(str, i, vars);
		vars->win_h = my_atoi(str, i, vars);
		test_str_params(str, *i, vars);
		vars->flag_res = 1;
		vars->check_params++;
		if (vars->win_w == 0 || vars->win_h == 0)
			check_error(vars, 3, MAP_ARGS_ERROR);
	}
}

static void		check_map_params(char *str, t_vars *vars, char mode)
{
	int i;

	i = -1;
	while (str[++i] != '\0')
	{
		if (mode == 'o')
		{
			if (str[i] == '1' && str[i + 1] == '1')
			{
				vars->flag = 1;
				break ;
			}
		}
		if (str[i] == 'R' || (str[i] == 'S' && str[i + 1] == ' '))
		{
			resolution_check(vars, str, &i);
			break ;
		}
		if ((check_all_params(str, vars, i) == 1)
		|| (check_all_params_sec(str, vars, i) == 1)
		|| (check_floor(str, vars, i) == 1) || (check_ceill(str, vars, i) == 1))
			break ;
	}
}

static void		create_map(t_vars *vars)
{
	int		i;
	t_list	*tmp;

	i = 0;
	tmp = vars->head;
	while (tmp != NULL)
	{
		check_map_params(tmp->content, vars, 'o');
		if (vars->flag == 1)
			break ;
		if (check_first_char(tmp->content) == 0)
			check_error(vars, 3, MAP_ARGS_ERROR);
		check_map_params(tmp->content, vars, 'a');
		tmp = tmp->next;
	}
	if (!(vars->map = (char **)ft_calloc(ft_lstsize(tmp) + 1, sizeof(char *))))
		check_error(vars, 2, MEMORY_ERROR);
	while (tmp != NULL)
	{
		vars->map[i] = tmp->content;
		i++;
		tmp = tmp->next;
	}
}

static void		check_flags(t_vars *vars)
{
	if (vars->win_w > 1920)
		vars->win_w = 1920;
	if (vars->win_h > 1080)
		vars->win_h = 1080;
	if (vars->check_params == 16 && vars->flag == 1 && vars->flag_res == 1
	&& vars->flag_w == 1 && vars->flag_n == 1 && vars->flag_s == 1
	&& vars->flag_e == 1 && vars->flag_ceill == 1
	&& vars->flag_floor == 1 && vars->flag_p == 1)
	{
		get_player_xy(vars);
		check_map_all(vars);
	}
	else
	{
		check_error(vars, 3, MAP_ARGS_ERROR);
	}
}

void			parse_map(t_vars *vars, int fd)
{
	char	*line;
	int		check;

	check = 0;
	line = NULL;
	while ((check = get_next_line(fd, &line)) != 0)
	{
		if (check == -1)
			check_error(vars, 2, FD_ERROR);
		if (!(vars->tmp = ft_lstnew(line)))
		{
			free(line);
			check_error(vars, 2, MEMORY_ERROR);
		}
		ft_lstadd_back(&(vars->head), vars->tmp);
	}
	if (!(vars->tmp = ft_lstnew(line)))
	{
		free(line);
		check_error(vars, 2, MEMORY_ERROR);
	}
	ft_lstadd_back(&(vars->head), vars->tmp);
	create_map(vars);
	check_flags(vars);
}
