/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_sprite.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/20 17:43:50 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/24 19:23:39 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

static void		draw_line_sprite(t_vars *vars, int stripe, int start, int end)
{
	float	k;
	int		starty;
	int		endy;
	int		begin;

	starty = start;
	vars->sprite_start = 0;
	k = vars->img_sprite.img_h / (float)(end - start);
	if (starty < 0)
	{
		vars->sprite_start = -starty;
		starty = 0;
	}
	endy = starty + (end - start) + vars->sprite_start;
	if (endy >= vars->win_h)
		endy = vars->win_h - 1;
	begin = starty;
	while (starty < endy)
	{
		vars->color = get_pixel_clr(vars, vars->img_x,
			(int)((starty - begin + vars->sprite_start) * k));
		if (vars->color)
			my_mlx_pxl_put(vars, stripe, starty, vars->color);
		starty++;
	}
}

static void		get_sprite_xy(t_vars *vars, int sprite_scr_x,
								int sprite_width, float *zbuff)
{
	int stripe;

	vars->sprite_startx = sprite_scr_x - sprite_width / 2;
	if (vars->sprite_startx < 0)
		vars->sprite_startx = 0;
	vars->sprite_endx = sprite_scr_x + sprite_width / 2;
	if (vars->sprite_endx >= vars->win_w)
		vars->sprite_endx = vars->win_w - 1;
	vars->mode = 'p';
	if (vars->ytransform > 0)
	{
		stripe = vars->sprite_startx;
		while (stripe < vars->sprite_endx)
		{
			vars->img_x = (int)(stripe - (sprite_scr_x - sprite_width / 2))
						* vars->img_sprite.img_w / sprite_width;
			if (vars->ytransform < zbuff[stripe])
				draw_line_sprite(vars, stripe,
								vars->sprite_starty, vars->sprite_endy);
			stripe++;
		}
	}
}

static void		check_start_end_sprite(t_vars *vars, float *zbuff)
{
	int sprite_scr_x;
	int sprite_height;
	int sprite_width;
	int move_screen;

	move_screen = (int)(vars->move / vars->ytransform);
	sprite_scr_x = (int)((vars->win_w / 2)
				* (1 + vars->xtransform / vars->ytransform));
	sprite_height = abs((int)(vars->win_h / vars->ytransform))
					/ vars->move_height;
	vars->sprite_starty = vars->win_h / 2 - sprite_height / 2 + move_screen;
	vars->sprite_endy = vars->win_h / 2 + sprite_height / 2 + move_screen;
	sprite_width = abs((int)(vars->win_h / vars->ytransform))
					/ vars->move_width;
	get_sprite_xy(vars, sprite_scr_x, sprite_width, zbuff);
}

void			draw_sprite(t_vars *vars, float *zbuff)
{
	t_sprite	*tmp;
	double		x_sprite;
	double		y_sprite;
	double		sprite;

	sprite_choice_sort(vars->lst, vars);
	tmp = vars->lst;
	while (tmp != NULL)
	{
		x_sprite = tmp->x - vars->player_x;
		y_sprite = tmp->y - vars->player_y;
		sprite = sqrt(x_sprite * x_sprite + y_sprite * y_sprite);
		vars->gamma = atan(y_sprite / x_sprite);
		if (x_sprite < 0)
			vars->gamma += M_PI;
		vars->alpha = atan(vars->dir_y / vars->dir_x);
		if (vars->dir_x < 0)
			vars->alpha += M_PI;
		vars->beta = vars->gamma - vars->alpha;
		vars->xtransform = sprite * sin(vars->beta);
		vars->ytransform = sprite * cos(vars->beta);
		check_start_end_sprite(vars, zbuff);
		tmp = tmp->next;
	}
}
