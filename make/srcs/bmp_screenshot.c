/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bmp_screenshot.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/14 22:46:03 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/30 11:51:03 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

static void		free_my(t_vars *vars)
{
	if (vars->map)
		free(vars->map);
	if (vars->head)
		free_tmp(&(vars->head));
	if (vars->lst)
		free_sprite(&(vars->lst));
	if (vars->img_s.img_path != NULL && vars->flag_s == 1)
		free(vars->img_s.img_path);
	if (vars->img_n.img_path != NULL && vars->flag_n == 1)
		free(vars->img_n.img_path);
	if (vars->img_w.img_path != NULL && vars->flag_w == 1)
		free(vars->img_w.img_path);
	if (vars->img_e.img_path != NULL && vars->flag_e == 1)
		free(vars->img_e.img_path);
	if (vars->img_sprite.img_path != NULL && vars->flag_p == 1)
		free(vars->img_sprite.img_path);
}

int				cmp_str(t_vars *vars, char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] != vars->check_argv[i])
		{
			free(vars->check_argv);
			vars->check_argv = NULL;
			check_error(vars, 1, MAP_ARGS_ERROR);
		}
		i++;
	}
	free(vars->check_argv);
	vars->check_argv = NULL;
	return (1);
}

int				check_first_char(char *c)
{
	int		i;
	int		j;
	char	*str;

	str = "FCSNWER\n\0";
	i = 0;
	j = 0;
	while (str[j])
	{
		if (c[i] == str[j] || c[i] == 0)
			return (1);
		j++;
	}
	return (0);
}

int				cmp_str_argv(char *str1, char *str2)
{
	int i;
	int n;

	i = 0;
	while (str1[i] != '\0')
	{
		if (str1[i] == '.')
		{
			str1 += i;
			break ;
		}
		i++;
	}
	i = 0;
	n = ft_strlen(str1);
	while (i < n && str1[i] != '\0' && str2[i] != '\0')
	{
		if (str1[i] != str2[i])
			return ((unsigned char)str1[i] - (unsigned char)str2[i]);
		i++;
	}
	if (i < n)
		return ((unsigned char)str1[i] - (unsigned char)str2[i]);
	return (0);
}

void			make_screenshot(t_vars *vars)
{
	int				size;

	size = vars->win_w * vars->win_h * vars->img_win.bits_per_pixel / 8 + 54;
	if (!(vars->bmp = (unsigned char *)ft_calloc(size, sizeof(char))))
		check_error(vars, 3, MLX_ERROR);
	ft_memcpy(&(vars->bmp[0]), "BM", 2);
	vars->fd_bmp = 54;
	ft_memcpy(&(vars->bmp[10]), &(vars->fd_bmp), 4);
	vars->fd_bmp = 40;
	ft_memcpy(&(vars->bmp[14]), &(vars->fd_bmp), 4);
	ft_memcpy(&(vars->bmp[18]), &vars->win_w, 4);
	vars->fd_bmp = -vars->win_h;
	ft_memcpy(&(vars->bmp[22]), &(vars->fd_bmp), 4);
	vars->fd_bmp = 1;
	ft_memcpy(&(vars->bmp[26]), &(vars->fd_bmp), 2);
	ft_memcpy(&(vars->bmp[28]), &vars->img_win.bits_per_pixel, 2);
	ft_memcpy(&(vars->bmp[54]), vars->img_win.addrs, size - 54);
	vars->fd_bmp = open("screenshot.bmp", O_CREAT | O_WRONLY | O_TRUNC, 0644);
	vars->fd_bmp = write(vars->fd_bmp, vars->bmp, size);
	if (vars->fd_bmp == -1)
		check_error(vars, 3, FD_ERROR);
	free(vars->bmp);
	close(vars->fd_bmp);
	free_my(vars);
	exit(0);
}
