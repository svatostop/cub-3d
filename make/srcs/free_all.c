/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_all.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 15:55:30 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/24 19:19:41 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

void			free_tmp(t_list **head)
{
	t_list *tmp;
	t_list *tmp1;

	tmp = *head;
	while (tmp != NULL)
	{
		tmp1 = tmp->next;
		if (tmp->content)
			free(tmp->content);
		free(tmp);
		tmp = tmp1;
	}
}

void			free_sprite(t_sprite **head)
{
	t_sprite *tmp;
	t_sprite *tmp1;

	tmp = *head;
	while (tmp != NULL)
	{
		tmp1 = tmp->next;
		free(tmp);
		tmp = tmp1;
	}
}

static void		print_error(int err)
{
	ft_putendl_fd("Error", 1);
	if (err == FD_ERROR)
		ft_putendl_fd("--____FD___ERROR___--", 1);
	if (err == ARGS_ERROR)
		ft_putendl_fd("--____ARGS___ERROR___--", 1);
	if (err == MEMORY_ERROR)
		ft_putendl_fd("--____MEMORY___ERROR___--", 1);
	if (err == MAP_ARGS_ERROR)
		ft_putendl_fd("--____MAP_ARGS___ERROR___--", 1);
	if (err == MAP_ERROR)
		ft_putendl_fd("--____MAP___ERROR___--", 1);
	if (err == MLX_ERROR)
		ft_putendl_fd("--____MLX___ERROR___--", 1);
	exit(0);
}

void			check_error(t_vars *vars, int check, int err)
{
	if (check == 2)
	{
		if (vars->head)
			free_tmp(&(vars->head));
	}
	else if (check == 3)
	{
		if (vars->img_s.img_path != NULL && vars->flag_s == 1)
			free(vars->img_s.img_path);
		if (vars->img_n.img_path != NULL && vars->flag_n == 1)
			free(vars->img_n.img_path);
		if (vars->img_w.img_path != NULL && vars->flag_w == 1)
			free(vars->img_w.img_path);
		if (vars->img_e.img_path != NULL && vars->flag_e == 1)
			free(vars->img_e.img_path);
		if (vars->img_sprite.img_path != NULL && vars->flag_p == 1)
			free(vars->img_sprite.img_path);
		if (vars->map)
			free(vars->map);
		if (vars->head)
			free_tmp(&(vars->head));
		if (vars->lst)
			free_sprite(&(vars->lst));
	}
	print_error(err);
}
