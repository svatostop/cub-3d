/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_map_args.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/08 09:49:20 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/25 15:28:32 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "h_mlx.h"

int			test_str_params(char *str, int i, t_vars *vars)
{
	while (str[i] != '\0')
	{
		if (str[i] >= 33 && str[i] <= 126)
		{
			check_error(vars, 3, MAP_ARGS_ERROR);
		}
		i++;
	}
	return (1);
}

int			check_floor(char *str, t_vars *vars, int i)
{
	if (str[i] == 'F')
	{
		vars->img_floor_ceill.r_c = my_atoi(str, &i, vars);
		while (str[i] == ',' || str[i] == ' ' || str[i] == '\t')
			i++;
		vars->img_floor_ceill.g_c = my_atoi(str, &i, vars);
		while (str[i] == ',' || str[i] == ' ' || str[i] == '\t')
			i++;
		vars->img_floor_ceill.b_c = my_atoi(str, &i, vars);
		test_str_params(str, i, vars);
		vars->flag_ceill = 1;
		vars->check_params++;
		if (vars->img_floor_ceill.r_c > 255
		|| vars->img_floor_ceill.g_c > 255 || vars->img_floor_ceill.b_c > 255)
		{
			check_error(vars, 3, MAP_ARGS_ERROR);
		}
		return (1);
	}
	return (0);
}

int			check_ceill(char *str, t_vars *vars, int i)
{
	if (str[i] == 'C')
	{
		vars->img_floor_ceill.r = my_atoi(str, &i, vars);
		while (str[i] == ',' || str[i] == ' ' || str[i] == '\t')
			i++;
		vars->img_floor_ceill.g = my_atoi(str, &i, vars);
		while (str[i] == ',' || str[i] == ' ' || str[i] == '\t')
			i++;
		vars->img_floor_ceill.b = my_atoi(str, &i, vars);
		test_str_params(str, i, vars);
		vars->flag_floor = 1;
		vars->check_params++;
		if (vars->img_floor_ceill.r > 255
		|| vars->img_floor_ceill.g > 255 || vars->img_floor_ceill.b > 255)
			check_error(vars, 3, MAP_ARGS_ERROR);
		return (1);
	}
	return (0);
}

int			check_all_params_sec(char *str, t_vars *vars, int i)
{
	char *tm;

	tm = NULL;
	if (str[i] == 'S' && str[i + 1] == 'O')
	{
		i += 3;
		tm = vars->img_s.img_path;
		vars->img_s.img_path = ft_strtrim(str + i, "\t ");
		free(tm);
		vars->flag_s = 1;
		vars->check_params++;
		return (1);
	}
	if (str[i] == 'E' && str[i + 1] == 'A')
	{
		i += 3;
		tm = vars->img_e.img_path;
		vars->img_e.img_path = ft_strtrim(str + i, "\t ");
		free(tm);
		vars->flag_e = 1;
		vars->check_params++;
		return (1);
	}
	return (0);
}

int			check_all_params(char *str, t_vars *vars, int i)
{
	char *tm;

	tm = NULL;
	if (str[i] == 'N' && str[i + 1] == 'O')
	{
		i += 3;
		tm = vars->img_n.img_path;
		vars->img_n.img_path = ft_strtrim(str + i, "\t ");
		free(tm);
		vars->flag_n = 1;
		vars->check_params++;
		return (1);
	}
	if (str[i] == 'W' && str[i + 1] == 'E')
	{
		i += 3;
		tm = vars->img_w.img_path;
		vars->img_w.img_path = ft_strtrim(str + i, "\t ");
		free(tm);
		vars->flag_w = 1;
		vars->check_params++;
		return (1);
	}
	return (0);
}
