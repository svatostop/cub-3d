/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   h_mlx.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/13 16:41:08 by lgorilla          #+#    #+#             */
/*   Updated: 2020/10/25 15:42:07 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef H_MLX_H
# define H_MLX_H

# include <fcntl.h>
# include <math.h>
# include "gnl/get_next_line.h"
# include "libft/libft.h"
# include "mlx/mlx.h"

typedef struct				s_sprite
{
	float					x;
	float					y;
	struct s_sprite			*next;
}							t_sprite;

typedef struct				s_img
{

	void					*img;

	char					*img_path;
	int						r;
	int						g;
	int						b;
	int						r_c;
	int						g_c;
	int						b_c;

	int						img_w;
	int						img_h;

	char					*addrs;
	int						bits_per_pixel;
	int						line_length;
	int						endian;

}							t_img;

typedef struct				s_vars
{
	void					*mlx;
	char					**map;
	void					*mlx_win;
	int						win_w;
	int						win_h;
	unsigned char			*bmp;

	t_sprite				*lst;
	int						sprites;
	int						sprite_startx;
	int						sprite_starty;
	int						sprite_start;
	int						sprite_endx;
	int						sprite_endy;
	double					move_height;
	double					move_width;
	double					move;

	char					mode;
	int						check_params;
	int						fd_bmp;
	int						flag;
	int						flag_res;
	int						flag_w;
	int						flag_n;
	int						flag_s;
	int						flag_e;
	int						flag_p;
	int						flag_win_h;
	int						flag_win_w;
	int						flag_floor;
	int						flag_ceill;
	float					img_x;
	float					perp_dist;

	float					player_x;
	float					player_y;

	char					*check_argv;
	int						argv_flag;

	double					xtransform;
	double					ytransform;
	double					gamma;
	double					alpha;
	double					beta;
	int						color;

	int						map_x;
	int						map_y;
	float					dir_x;
	float					dir_y;
	float					a_x;
	float					a_y;
	float					dist_x;
	float					dist_y;
	float					step_x;
	float					step_y;
	int						view_h;

	t_img					img_win;
	t_img					img_n;
	t_img					img_e;
	t_img					img_w;
	t_img					img_s;
	t_img					img_sprite;
	t_img					img_floor_ceill;

	t_list					*head;
	t_list					*tmp;
}							t_vars;

enum						e_errors
{
	MAP_ERROR = 1,
	ARGS_ERROR,
	MAP_ARGS_ERROR,
	FD_ERROR,
	MEMORY_ERROR,
	MLX_ERROR
};

void						my_mlx_pxl_put(t_vars *vars,
							int x, int y, int color);
int							get_pixel_clr(t_vars *vars, int x, int y);
int							create_trgb(int t, int r, int g, int b);
int							event_proc(int keycode, t_vars *vars);
void						draw_map(t_vars *vars);
void						draw_line(t_vars *vars, int x);
void						render_next_frame(t_vars *vars);
void						draw_floor(t_vars *vars);
int							my_atoi(const char *str, int *i, t_vars *vars);
void						parse_map(t_vars *vars, int fd);
int							check_floor(char *str, t_vars *vars, int i);
int							check_ceill(char *str, t_vars *vars, int i);
void						get_player_xy(t_vars *vars);
int							cmp_str(t_vars *vars, char *str);
void						make_screenshot(t_vars *vars);
void						free_tmp(t_list **head);
int							test_str_params(char *str, int i, t_vars *vars);
void						check_error(t_vars *vars, int check, int err);
void						check_fd_args(int *fd,
							char **argv, int argc, t_vars *vars);
void						check_map_all(t_vars *vars);
int							check_all_params(char *str, t_vars *vars, int i);
int							check_all_params_sec(char *str,
							t_vars *vars, int i);
int							destroy_win(t_vars *vars);
int							check_first_char(char *c);
int							cmp_str_argv(char *str1, char *str2);
void						free_sprite(t_sprite **head);
void						draw_sprite(t_vars *vars, float *zbuffer);
void						sprite_choice_sort(t_sprite *head, t_vars *vars);
t_sprite					*spritenew(t_vars *vars, float x, float y);
void						spriteadd_back(t_sprite **head, t_sprite *new);
void						check_event(t_vars *vars, char mode);
void						check_event_sec(t_vars *vars, char mode);
void						check_event_third(t_vars *vars, char mode);

#endif
